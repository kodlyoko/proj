﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryGap
{
    class Program
    {
        public int solution(int N)
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            //the longest binary gap return
            if (N == 1 || N == 2)
            {
                return 0;
            }
            var binaryNumber = new List<int>();
           
            while (N != 0)
            {
                binaryNumber.Add(N % 2);
                N = N / 2;
            }
            binaryNumber.Reverse();
            //finding binary gap/gaps
            int k = 0;
            var zeroNumber = new List<int>();

          for(int w= 0; w< binaryNumber.Count()-1; w++)
            {

                    if (binaryNumber[w] == 1 && binaryNumber[w + 1] == 0)
                    {
                        for (int n = w + 2; n < binaryNumber.Count(); n++)
                        {
                            if (binaryNumber[n] == 1)
                            {
                                var gaplength = n - (w + 1);
                                zeroNumber.Add(gaplength);
                                break;
                            }
                        }
                    }

            }
            if(zeroNumber.Count() == 0)
            {
                return 0;
            }
                var maxValue = zeroNumber.Max();
                return maxValue;
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            var result = program.solution(328);

            Console.WriteLine(result);
        }
    }
    
}
