﻿using System;

namespace CyclicRotation
{
    class Program
    {
        public int[] solution(int[] A, int K)
        {
            var size = A.Length;
            for (int i = 0; i < K; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int b = A[size - 1];//last element
                    A[size - 1] = A[j];//put in actual
                    A[j] = b;//sign last element
                }
            }

            return A;
            
        }
        static void Main(string[] args)
        {
            var table = new int[] { 0, 1, 4, 1, 4 };
            Console.WriteLine("[{0}]", string.Join(",", table));
            var k = new Program();
            var result = k.solution(table,2);
            Console.WriteLine("[{0}]", string.Join(",", result));
        }
    }
}
