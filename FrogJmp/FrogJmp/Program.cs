﻿using System;

namespace FrogJmp
{
    class Program
    {
        public int solution(int X, int Y, int D)
        {
            if(X==Y)
            {

                return 0;
            }
            var distance = Y - X;

            int jumpsNumber= distance/D;
            if(distance % D == 0)
            {
                return jumpsNumber;
            }
            else
            {
                return jumpsNumber + 1;
            }

        }
        static void Main(string[] args)
        {
         
            var k = new Program();
            var result = k.solution(1,85,300000);
            Console.WriteLine(result);
        }
    }
}

